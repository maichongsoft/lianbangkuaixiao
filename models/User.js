/**
 * 脉冲软件
 * http://maichong.it
 * Created by Liang on 15/9/22.
 * liang@maichong.it
 */

'use strict';

const co = require('co');
const keystone = require('keystone');
const Types = keystone.Field.Types;

const config = require('../lib/config');
const random = require('../lib/random');
const round = require('number-round');

const List = new keystone.List('User', {
	label: '用户',
	plural: '用户',
	map: {
		name: 'tel'
	},
	searchFields: 'tel,invitation,name,email',
	defaultSort: '-createdAt',
	defaultColumns: 'avatar,tel,name,email,balance,invitation,createdAt',
	nocreate: true,
	nodelete: true
});

List.add({
	tel: {
		label: '手机',
		type: String,
		initial: true,
		index: true,
		noedit: true
	},
	avatar: {
		label: '头像',
		type: Types.AliyunImage,
		nosort: true,
		nofilter: true,
		noedit: true
	},
	name: {
		label: '姓名',
		type: String,
		default: ''
	},
	email: {
		label: '邮箱',
		type: Types.Email,
		initial: true,
		index: true
	},
	address: {
		label: '地址',
		type: String,
		noedit: true
	},
	password: {
		label: '密码',
		type: Types.Password,
		initial: true,
		required: true
	},
	createdAt: {
		label: '创建时间',
		type: Date,
		noedit: true
	},
	ip: {
		label: '注册IP',
		type: String,
		noedit: true
	},
	senderStatus: {
		label: '发单者状态',
		type: Types.Select,
		numeric: true,
		options: [{
			label: '未申请',
			value: 0
		}, {
			label: '申请中',
			value: 1
		}, {
			label: '通过',
			value: 2
		}],
		noedit: true,
		default: 0
	},
	courierStatus: {
		label: '接单者状态',
		type: Types.Select,
		numeric: true,
		options: [{
			label: '未申请',
			value: 0
		}, {
			label: '申请中',
			value: 1
		}, {
			label: '通过',
			value: 2
		}],
		noedit: true,
		default: 0
	},
	balance: {
		label: '余额',
		type: Number,
		default: 0
	},
	alipay: {
		label: '支付宝',
		type: String,
		default: ''
	},
	commissions: {
		label: '佣金总收入',
		type: Number,
		default: 0,
		noedit: true
	},
	token: {
		label: '令牌',
		type: String,
		default: '',
		noedit: true
	}
}, '权限', {
	blocked: {
		label: '屏蔽',
		type: Boolean
	},
	isAdmin: {type: Boolean, label: '管理员权限', index: true}
});

List.schema.methods.data = require('../lib/data');
List.schema.private = [
	'createdAt',
	'token',
	'password',
	'ip',
	'address',
	'blocked'
];

List.schema.virtual('canAccessKeystone').get(function () {
	return this.isAdmin;
});

List.schema.methods.checkPassword = function (password) {
	let user = this;

	return new Promise(function (resolve, reject) {
		user._.password.compare(password, function (error, result) {
			resolve(result);
		});
	});
};

List.schema.pre('save', function (next) {
	let me = this;
	if (!me.get('createdAt')) {
		me.set('createdAt', new Date);
	}
	if (!me.avatar || !me.avatar.link) {
		me.set('avatar', config.defaultAvatar)
	}

	me.balance = round(me.balance, 2);

	next();
});

List.relationship({
	ref: 'Order',
	path: 'orders'
});

List.relationship({
	ref: 'Income',
	path: 'incomes'
});

List.relationship({
	ref: 'Address',
	path: 'addresses'
});

List.relationship({
	ref: 'Withdraw',
	path: 'withdraws'
});

List.register();
