
#联邦快消 Node端

##安装说明

由于修改了底层的keystone框架，所以不能用npm简单安装。

####开发环境安装步骤

1. 克隆代码
> * git clone https://......


2. 切换到开发分支
> * git checkout develop

3. 安装项目npm依赖
> * npm install


4. 跳转到keystone目录，并安装keystone依赖
> * cd node_modules/keystone/
> * npm install



5. 跳转到 public/m 目录，安装微信端开发依赖
> * cd ../../public/m
> * npm install


6. 运行项目
> * cd ../..
> * nodemon lianbangkuaixiao.js


##各个目录文件介绍
* `/` 根目录
* `config/` Node服务器启动时读取的配置文件目录
> * `dev.js` 开发环境配置文件
> * `live.js` live环境配置文件

* `lib/` 项目中自定义的库
* `models/` 数据模型
* `node_modules/` Node服务器依赖的npm包

* `public/` 网站根目录
> * `m/` APP 根目录，访问地址 http://localhost:3018/m/
    `m/css/` APP CSS目录。此目录下文件由grunt生成，请勿修改。
    `m/img/` 图片目录
    `m/js/` js目录。此目录下文件由grunt生成，请勿修改。
    `m/less/` LESS
    `m/lib/` ionic等第三方库
    `m/node_modules/` APP 开发所依赖的npm包 
    `m/src/` JS源码
    `m/src/controller/` AngularJS控制器
    `m/src/directive/` AngularJS指令
    `m/src/service/` AngularJS服务模块
    `m/views/` AngularJS 视图
    `m/index.html` AngularJS 入口
    `m/Gruntfile.js` grunt 打包配置文件
> * `*` fonts、images等其他文件无用

* `routes/` 路由控制器，相当于PHP框架中的controllers

* `templates/` 模板目录

* `updates/` 升级包目录

* `.env` 环境配置文件，如果不存在，复制 .env-simple

* `.env-simple` 环境配置示例文件

* `lianbangkuaixiao.js` node服务器启动文件

* `Gruntfile.js` keystone生成的Grunt配置文件，此文件暂时无用

* `nodemon.js` nodemon配置文件

* `package.js` 项目包信息

