/**
 * 开发服务器配置 http://sucarf.dev.maichong.it
 */

var config = require('./live'),
	_ = config._;

_.extend(config, {});

_.extend(config.init, {
	env: 'development'
});

module.exports = config;
