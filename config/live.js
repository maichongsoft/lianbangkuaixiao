/**
 * 在线服务器配置
 */
var swig = require('swig');

swig.setFilter('ng', function (input) {
	return '{{' + input + '}}';
});

var oss = require('../lib/oss.js');

var mime = require('mime');

var _ = require('underscore');

var ObjectId = null;

swig.setDefaults({
	cache: false
});

var aliyunConfig = {
	accessKeyId: 'DHhxp68X5Y0l3SR1',
	secretAccessKey: 'H9nhdYwFsZd4KQUWVd1w4a7NVQwz4n',
	endpoint: 'http://oss-cn-beijing.aliyuncs.com',
	apiVersion: '2013-10-15',
	urlPrefix: 'http://xjqm.img.maichong.it/',
	prefix: '',
	//folderFormat: "YYYY/MM/DD/",
	folderFormat: 'YYMM/',
	extAllowed: function (file) {
		var ext = mime.extension(file.type).replace('jpeg', 'jpg');
		file.ext = ext;
		return _.indexOf(['jpg', 'png', 'jpeg', 'gif'], ext) > -1;
	},
	fileName: function (file) {
		if (!ObjectId) {
			ObjectId = require('keystone').mongoose.Types.ObjectId;
		}
		return (new ObjectId()).toHexString() + '.' + mime.extension(file.type).replace('jpeg', 'jpg');
	},
	upload: {
		Bucket: 'xjqm',
		AccessControlAllowOrigin: '',
		ContentType: 'text/plain',
		CacheControl: 'no-cache', // 参考: http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.9
		ContentDisposition: '', // 参考: http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html#sec19.5.1
		ContentEncoding: 'utf-8', // 参考: http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.11
		//ServerSideEncryption: 'AES256',
		Expires: 1732896000
	}
};

var config = module.exports = {
	_: _,
	init: {

		//'headless':true,

		name: 'Lianbangkuaixiao',
		brand: '联邦快消',

		port: 3018,

		//less: 'public',
		static: 'public',
		favicon: 'public/favicon.ico',
		views: 'templates/views',
		'view engine': 'swig',

		'custom engine': swig.renderFile,

		emails: 'templates/emails',

		'auto update': true,
		session: true,
		auth: true,
		'user model': 'User',
		'cookie secret': 'eim8T!bX1IH5m^DLkPLV#3vqN4f#yR#74Uf^6xjzNBMCQko$#ehmD*dlA1sFW!Hh',

		'aliyun oss': oss(aliyunConfig),

		env: 'production', //development or production

		'session options': {
			key: 'maichong.sid'
		},

		'cookie signin': true,

		'wysiwyg aliyun images': true,

		'back url': '/'

	},
	set: {},
	aliyun: aliyunConfig,
	imgUrl: 'http://xjqm.img.maichong.it/',
	getImgUrl: function (img, width, quality) {
		var url = config.imgUrl + img.link;
		if (width) {
			url += '@' + width + 'w_' + (quality || '90') + 'Q_1x.jpg';
		}
		return url;
	},
	defaultAvatar: {
		link: 'avatar.jpg',
		time: new Date(),
		size: 337981,
		ext: 'jpg',
		default: true
	},
	baidu: {
		ak: 'n9v1AMuFEX6AtX34KCI7SGhO',
		sk: 'K1f0YdkGUb6ziA5Oz8hVETOSaH2XrWpN'
	}

};
