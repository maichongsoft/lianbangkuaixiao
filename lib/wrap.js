var co = require('co');

module.exports = function wrap(gen) {
	var fn = co.wrap(gen);

	if (gen.length === 4) {
		return function (err, req, res, next) {
			return fn(err, req, res, next).catch(next);
		}
	}

	return function (req, res, next) {
		return fn(req, res, next).catch(function (error) {
			var msg = (new Date).toLocaleString() + ' Catch';
			if (error instanceof Error) {
				msg += error.stack;
			} else {
				msg += 'Error: ' + error;
			}
			console.error(msg);
			res.error(error);
		});
	};
};
