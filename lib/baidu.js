/**
 * 脉冲软件
 * http://maichong.it
 * Created by Liang on 15/10/12.
 * liang@maichong.it
 */

'use strict';

const request = require('request');
const md5 = require('MD5');
const GPSCodec = require('./GPSCodec');
const round = require('number-round');
const _ = require('underscore');

const config = require('./config');

const AK = config.baidu.ak;
const SK = config.baidu.sk;

/**
 * 地理位置名称转成GPS坐标
 * @param address
 * @returns {Promise}
 */
exports.addressToGps = function (address) {
	return new Promise(function (resolve, reject) {
		let url = getUrl('/geocoder/v2/', {
			address: address
		});
		request(url, function (error, res, body) {
			let json = JSON.parse(body);
			if (json.status != 0) {
				return resolve(null);
			}

			let lng = json.result.location.lng;
			let lat = json.result.location.lat;

			let gps = GPSCodec.bd_decrypt(lat, lng);

			resolve([gps.lon, gps.lat]);

		})
	});
};

/**
 * 搜索地理位置
 * @param keyword
 * @param region [optional]区域
 * @returns {Promise}
 */
exports.search = function (keyword, region) {
	return new Promise(function (resolve, reject) {
		let url = getUrl('/place/v2/search', {
			q: keyword,
			region: region || '郑州市'
		});
		request(url, function (error, res, body) {

			let json = JSON.parse(body);
			if (json.status != 0) {
				return resolve(null);
			}

			let results = [];

			for (let i in json.results) {
				let item = json.results[i];
				if (item.num || !item.address) {
					continue;
				}
				let gps = GPSCodec.bd_decrypt(item.location.lat, item.location.lng);
				results.push([item.name, round(gps.lon, 6), round(gps.lat, 6), item.address]);
			}

			resolve(results);

		})
	});
};

/**
 * GPS位置转成详细地址
 * 输入的经纬度为火星坐标
 * @param lng
 * @param lat
 * @returns {Promise}
 */
exports.gpsToLocation = function (lng, lat) {
	return new Promise(function (resolve, reject) {
		let url = getUrl('/geocoder/v2/', {
			location: lat + ',' + lng,
			pois: 1,
			coordtype: 'gcj02ll' //火星坐标
		});

		request(url, function (error, res, body) {

			let json = JSON.parse(body);
			if (json.status != 0 || !json.result || !json.result.addressComponent) {
				return resolve(null);
			}

			let result = json.result.addressComponent;
			result.desc = json.result.sematic_description;
			result.pois = [];
			for (let i in json.result.pois) {
				let p = json.result.pois[i];
				let gps = GPSCodec.bd_decrypt(p.point.y, p.point.x);
				result.pois.push({
					name: p.name,
					addr: p.addr,
					lng: round(gps.lon, 6),
					lat: round(gps.lat, 6)
				});
			}

			if (result.desc) {
				result.desc = result.desc.replace('附近0米', '附近');
			}
			delete result.country;
			delete result.direction;
			delete result.distance;
			delete result.street_number;
			delete result.country_code;

			resolve(result);

		});
	});
};

/**
 * 将IP转化成GPS位置，callback返回的位置为火星坐标
 * @param ip
 * @returns {Promise}
 */
exports.ipToLocation = function (ip) {
	return new Promise(function (resovle, reject) {
		let url = getUrl('/location/ip', {
			ip: ip,
			coor: 'bd09ll'
		});

		request(url, function (error, res, body) {

			let json = JSON.parse(body);

			if (json.status != 0 || !json.content) {
				return resovle(null);
			}

			let result = json.content.address_detail;
			delete result.street;
			delete result.street_number;

			let gps = GPSCodec.bd_decrypt(json.content.point.y, json.content.point.x);

			result.lng = round(gps.lon, 6);
			result.lat = round(gps.lat, 6);

			resovle(result);
		})
	});
};

/**
 * 获取两点间的路径
 * @param fromLng
 * @param fromLat
 * @param toLng
 * @param toLat
 * @returns {Promise}
 */
exports.direction = function (fromLng, fromLat, toLng, toLat) {
	return new Promise(function (resovle, reject) {
		let url = getUrl('/direction/v1', {
			origin: fromLat + ',' + fromLng,
			destination: toLat + ',' + toLng,
			mode: 'driving',
			output: 'json',
			coord_type: 'gcj02',
			region: '全国',
			timestamp: parseInt(Date.now() / 1000)
		});

		request(url, function (error, res, body) {

			let json = JSON.parse(body);

			if (json.status != 0 || !json.result || !json.result.routes) {
				return resovle(null);
			}
			delete json.result.taxi;

			resovle(json.result);
		});
	});
};

//计算参数签名
function getSn(path, data) {

	let arr = [];

	_.each(_.keys(data), function (key) {
		arr.push(key + '=' + encodeURIComponent(data[key]));
	});

	let querystring = path + '?' + arr.join('&') + SK;

	return md5(encodeURIComponent(querystring));
}

//拼装百度api链接地址
function getUrl(path, data) {
	data.ak = AK;
	data.output = 'json';

	let url = 'http://api.map.baidu.com' + path + '?';

	for (let key in data) {
		url += key + '=' + encodeURIComponent(data[key]) + '&';
	}

	url += 'sn=' + getSn(path, data);

	//console.log(url);

	return url;
}
