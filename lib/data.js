var _ = require('underscore');
var config = require('./config');

function pick() {
	return _.pick.apply(_, [this].concat(Array.prototype.slice.call(arguments)));
};

function omit() {
	return _.omit.apply(_, [this].concat(Array.prototype.slice.call(arguments)));
};

function getObjectData(value, record, key) {
	if (value == null) {
		return value;
	}
	if (value instanceof Date) {
		//时间转化为时间戳
		value = parseInt(value.getTime() / 1000);
	} else if (value instanceof Array) {
		//数组数据
		var newValue = [];
		for (var i in value) {
			if (i * 1 != i) {
				continue;
			}
			var val = value[i];
			if (typeof val == 'object') {
				val = getObjectData(val, value, key);
			}
			newValue[i] = val;
		}
		return newValue;
	} else if (value.data && typeof value.data == 'function') {
		//如果也有data 函数，判定为document
		value = value.data();
	} else if (record && key && record._ && record._[key] && record._[key].format) {
		value = record._[key].format();
	} else if (value.link && value.time && value.ext) {
		//强制判定为AliyunImage
		return config.getImgUrl(value);
	} else {
		//console.log(value, key, record);
	}
	return value;
}

module.exports = function () {
	var data = {
		id: this.id,
		pick: pick,
		omit: omit
	};
	var keys = _.keys(this.schema.tree);
	if (this.schema.private) {
		keys = _.difference(keys, this.schema.private);
	}
	for (var i in keys) {
		var key = keys[i];
		if (key[0] == '_' || key == 'id' || key == 'list' || key.endsWith('RefList') || key.endsWith('Data') || key.endsWith('Label') || key.endsWith('Options') || key.endsWith('OptionsMap')) {
			continue;
		}
		var value = this.get(key);
		if (typeof value == 'object') {
			value = getObjectData(value, this, key);
		}
		data[key] = value;
	}
	return data;
};
