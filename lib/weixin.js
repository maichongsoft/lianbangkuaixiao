/**
 * weixin.js
 * @copyright Maichong Software Ltd. 2015 http://maichong.it
 * @date 2015-11-10
 * @author Liang <liang@maichong.it>
 */

'use strict';

const _ = require('underscore');
const co = require('co');
const request = require('./request');
const random = require('./random');
const sha1 = require('sha1');

function Weixin() {
	let me = this;
	me._config = {
		appid: '', //APP ID
		secret: '', //秘钥
		mch_id: '', //微信支付商户ID
		pay_key: '', //支付秘钥
		pay_notify_url: '' //支付通知地址
	};
}

exports = module.exports = Weixin.prototype = Weixin;

exports.init = function (config) {
	let me = this;
	_.extend(me._config, config);
	return me;
};

/**
 * 获取公众平台全局token
 * @returns {Promise}
 */
exports.getGlobalToken = function () {
	let me = this;
	return co(function* () {
		if (me._globalToken && Date.now() < me._globalTokenTime) {
			return me._globalToken;
		}
		let url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' + me._config.appid + '&secret=' + me._config.secret;
		let result = yield request(url);
		let data = JSON.parse(result.body);
		if (data.errcode) {
			throw new Error('Get weixin token failed:' + data.errmsg);
		}
		me._globalToken = data.access_token;
		me._globalTokenTime = Date.now() + data.expires_in * 1000;
		return me._globalToken;
	});
};

/**
 * 获取公众平台JSSDK ticket
 * @returns {Promise}
 */
exports.getTicket = function () {
	let me = this;
	return co(function* () {
		if (me._jsapiTicket && Date.now() < me._jsapiTicketTime) {
			return me._jsapiTicket;
		}
		let token = yield me.getGlobalToken();
		let url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=' + token + '&type=jsapi';
		let result = yield request(url);
		let data = JSON.parse(result.body);
		if (data.errcode) {
			throw new Error('Get weixin ticket failed:' + data.errmsg);
		}
		me._jsapiTicket = data.ticket;
		me._jsapiTicketTime = Date.now() + data.expires_in * 1000;
		return me._jsapiTicket;
	});
};

/**
 * 获取JSSDK初始化参数
 * @returns {Promise}
 */
exports.getJSConfig = function (url) {
	let me = this;
	return co(function*() {
		let data = {
			jsapi_ticket: '',
			noncestr: random.str(16),
			timestamp: time(),
			url: url
		};

		data.jsapi_ticket = yield me.getTicket();

		let arr = [];
		_.each(data, function (value, key) {
			arr.push(key + '=' + value);
		});

		data.signature = sha1(arr.join('&'));
		data.appId = me._config.appid;
		data.nonceStr = data.noncestr;
		data.jsApiList = [
			'checkJsApi',
			'onMenuShareTimeline',
			'onMenuShareAppMessage',
			'onMenuShareQQ',
			'onMenuShareWeibo',
			'hideMenuItems',
			'showMenuItems',
			'hideAllNonBaseMenuItem',
			'showAllNonBaseMenuItem',
			'translateVoice',
			'startRecord',
			'stopRecord',
			'onRecordEnd',
			'playVoice',
			'pauseVoice',
			'stopVoice',
			'uploadVoice',
			'downloadVoice',
			'chooseImage',
			'previewImage',
			'uploadImage',
			'downloadImage',
			'getNetworkType',
			'openLocation',
			'getLocation',
			'hideOptionMenu',
			'showOptionMenu',
			'closeWindow',
			'scanQRCode',
			'chooseWXPay',
			'openProductSpecificView',
			'addCard',
			'chooseCard',
			'openCard',
			'onMenuShareWeibo',
			'hideMenuItems',
			'showMenuItems',
			'hideAllNonBaseMenuItem',
			'showAllNonBaseMenuItem'
		];
		//data.debug = true; // config.init.env == 'development';
		//console.log(data);
		delete data.jsapi_ticket;
		delete data.noncestr;
		delete data.url;
		return data;
	});
};

/**
 * 用户网页授权后，将code转化为access_token
 * @param code
 * @returns {Promise}
 *
 * Promise.then返回数据格式为:
 *  {
 *    "access_token":"ACCESS_TOKEN",
 *    "expires_in":7200,
 *    "refresh_token":"REFRESH_TOKEN",
 *    "openid":"OPENID",
 *    "scope":"SCOPE",
 *    "unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"
 *  }
 */
exports.getAccessToken = function (code) {
	let me = this;
	return co(function*() {

		let url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' + me._config.appid + '&secret=' + me._config.secret + '&code=' + code + '&grant_type=authorization_code';

		let result = yield request(url);
		let data = JSON.parse(result.body);
		if (data.errcode) {
			throw new Error('Get weixin access_token failed:' + data.errmsg);
		}
		return data;
	});
};

/**
 * 获取微信关注者的身份信息
 * @param openid
 * @returns {Promise}
 */
exports.getFansInfo = function (openid) {
	let me = this;
	return co(function*() {
		let token = yield me.getGlobalToken();
		let url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=' + token + '&openid=' + openid + '&lang=zh_CN';
		let result = yield request(url);
		let data = JSON.parse(result.body);
		if (data.errcode) {
			throw new Error('Get weixin access_token failed:' + data.errmsg);
		}
		return data;
	});
};

/**
 * 下载文件
 * @returns {Promise}
 */
exports.downloadMedia = function (media_id) {
	let me = this;
	return co(function*() {
		let token = yield me.getGlobalToken();
		let url = 'http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=' + token + '&media_id=' + media_id;
		let result = yield request({
			encoding: null,
			url: url
		});

		if (!result.body) {
			throw new Error('No media data');
		}

		if (!result.headers['content-disposition']) {
			throw new Error('No media disposition');
		}

		let data = result.body;
		data.type = result.headers['content-type'];

		return data;
	});
};

/**
 * 查询订单
 * @param orderId
 * @returns {Promise}
 */
exports.orderquery = function (orderId) {
	let me = this;
	return co(function*() {

		let data = {
			appid: me._config.appid,
			mch_id: me._config.mch_id,
			nonce_str: random.str(16),
			out_trade_no: orderId
		};
		data.sign = getPaySign(data);

		let xml = data2xml(data);

		//console.log(xml);

		let result = yield request({
				method: 'POST',
				url: 'https://api.mch.weixin.qq.com/pay/orderquery',
				body: xml
			}
		);

		let json = yield xml2data(result.body);

		return json;
	});
};

/**
 * 支付接口
 * @param data 支付参数
 * @returns {Promise}
 */
exports.createPayReq = function (data) {

};

Weixin.call(exports);

//获取当前时间戳
function time() {
	return parseInt(Date.now() / 1000);
}

//获取MD5签名
function getMd5Sign(data, lastKey, lastValue) {
	let filted = {};
	for (let key in data) {
		let value = data[key] + '';
		if (!value) {
			continue;
		}
		filted[key] = value;
	}
	let keys = Object.keys(filted).sort();

	let arr = [];
	keys.forEach(function (key) {
		arr.push(key + '=' + filted[key]);
	});

	let string = arr.join('&');

	if (lastKey) {
		string += '&' + lastKey + '=' + lastValue;
	}

	return md5(string).toUpperCase();
}

//获取支付通用签名
function getPaySign(data, pay_key) {
	return getMd5Sign(data, 'key', pay_key);
}

//将数据转化为XML字符串
function data2xml(data) {
	let builder = new xml2js.Builder({
		cdata: true,
		rootName: 'xml'
	});

	return builder.buildObject(data);
}


//将XML转化为data数据
function xml2data(xml) {
	return new Promise(function (resolve, reject) {
		xml2js.parseString(xml, function (error, result) {
			if (error) {
				return reject(error);
			}
			var data = {};
			for (var key in result.xml) {
				var value = result.xml[key];
				data[key] = value && value.length == 1 ? value[0] : value;
			}
			resolve(data);
		});
	});
}
