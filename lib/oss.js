//阿里云OSS客户端
var ALI = require('aliyun-sdk'),
	_ = require('lodash'),
	mime = require('mime'),
	http = require('http'),
	fs = require('fs');


module.exports = function(config) {
	var oss = new ALI.OSS(config);

	oss.cfg = config;

	oss.uploadData = function(key, data, callback, options) {
		var ContentType = mime.lookup(key) || 'text/plain';
		options = options || {};
		var opt = _.extend({}, config.upload, options, {
			Key: key,
			Body: data,
			ContentType: ContentType
		});

		oss.putObject(opt, callback);
	};

	oss.uploadUrl = function(key, url, callback, options) {
		http.get(url, function(res) {
			var buffers = [];
			res.on('data', function(chunk) {
				buffers.push(chunk);
			});
			res.on('end', function() {
				var data = Buffer.concat(buffers);
				var length = data.length;
				oss.uploadData(key, data, function(error, result) {
					if (result) {
						result.length = length;
					}
					callback(error, result);
				}, options);
				buffers = data = null;
			});
		}).on('error', function() {
			callback(error);
		});
	};

	oss.upload = function(key, filePath, callback, options) {
		fs.readFile(filePath, function(err, data) {
			if (err) {
				console.error(err);
				return callback(err);
			}

			var ContentType = mime.lookup(key) || 'text/plain';
			options = options || {};
			var opt = _.extend({}, config.upload, options, {
				Key: key,
				Body: data,
				ContentType: ContentType
			});

			oss.putObject(opt, callback);
		});

	};
	return oss;
};


