module.exports = function (val, precision) {
	val = parseFloat(val) || 0;
	return parseFloat(val.toFixed(precision));
};
