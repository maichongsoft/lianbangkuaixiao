

var configFile = process.env.CONFIG_FILE;
if (!configFile) {
	console.log('没有指定配置文件，将使用默认配置：config/live.js ');
	configFile = 'config/live.js';
}

console.log('读取配置文件：' + configFile);

var config = require('../' + configFile);

module.exports = config;
