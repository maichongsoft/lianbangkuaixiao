/**
 * 脉冲软件
 * http://maichong.it
 * Created by Liang on 15/9/22.
 * liang@maichong.it
 */

'use strict';

const CHARS = 'ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789';

/**
 * 生成随机数字
 * @param min 最小
 * @param max 最大
 */
var exports = module.exports = function (min, max) {
	if (typeof max == 'undefined') {
		max = min;
		min = 0;
	}
	return parseInt(Math.random().toString().substr(2)) % (max - min) + min;
};

/**
 * 生成随机数字字符串
 * @param length
 */
exports.number = function (length) {
	return Math.random().toString().substr(2, length || 6);
};

/**
 * 生成随机字符串
 * @param length
 */
exports.string = function (length) {
	length = length || 12;
	let str = '';
	while (length > 1) {
		let idx = parseInt(Math.random() * 1000) % CHARS.length;
		str += CHARS[idx];
		length--;
	}
	return str;
};

/**
 * 快速生成随机字符串
 * @param length
 */
exports.str = function (length) {
	length = length || 12;
	let str = Math.random().toString(36).substr(2);
	if (str.length == length) {
		return str;
	}
	if (str.length > length) {
		return str.substr(0, length);
	}
	return exports.str(length - str.length) + str;
};
