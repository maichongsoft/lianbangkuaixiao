module.exports = function (grunt) {
	var cordovaPath = process.env.HOME + '/Cordova/qizhonghui/';
	require('load-grunt-tasks')(grunt);
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: ';',
				sourceMap: true
			},
			dev: {
				files: [{
					src: [
						'src/regeneratorRuntime.js',
						'src/start.js',
						'src/main.js',
						'src/dev/*.js',
						'src/factory/*.js',
						'src/service/*.js',
						'src/directive/*.js',
						'src/controller/*.js',
						'src/end.js'
					],
					dest: 'build/dev.js'
				}]
			},
			app: {
				files: [{
					src: [
						'src/regeneratorRuntime.js',
						'src/start.js',
						'src/main.js',
						'src/factory/*.js',
						'src/service/*.js',
						'src/directive/*.js',
						'src/controller/*.js',
						'src/end.js'
					],
					dest: 'build/app.js'
				}]
			},
			weixin: {
				files: [{
					src: [
						'src/start.js',
						'src/main.js',
						'src/weixin/*.js',
						'src/factory/*.js',
						'src/service/*.js',
						'src/directive/*.js',
						'src/controller/*.js',
						'src/end.js'
					],
					dest: 'build/weixin.js'
				}]
			}
		},
		babel: {
			options: {
				sourceMap: true
			},
			dev: {
				files: {
					'build/dev-babel.js': 'build/dev.js'
				}
			},
			app: {
				files: {
					'build/app-babel.js': 'build/app.js'
				}
			},
			debug: {
				files: {
					'build/app-debug-babel.js': 'build/app.js'
				}
			},
			weixin: {
				files: {
					'build/weixin-babel.js': 'build/weixin.js'
				}
			}
		},
		uglify: {
			app: {
				options: {
					sourceMap: false
				},
				files: {
					'build/app.min.js': 'build/app-babel.js'
				}
			},
			debug: {
				options: {
					sourceMap: false
				},
				files: {
					'build/app-debug.min.js': 'build/app-debug-babel.js'
				}
			},
			weixin: {
				options: {
					sourceMap: false
				},
				files: {
					'js/weixin.min.js': 'build/weixin-babel.js'
				}
			}
		},
		less: {
			base: {
				options: {
					paths: 'less',
					compress: false,
					sourceMap: true,
					banner: '/* liang@maichong.it */\n'
				},
				files: {
					'css/style.css': 'less/style.less'
				}
			}
		},
		sass: {
			dist: {
				options: {
					style: 'expanded'
				},
				files: {
					'css/ionic.css': 'lib/ionic/scss/ionic.scss'
				}
			}
		},
		cssmin: {
			options: {
				sourceMap: true,
				rebase: false,
				advanced: false
			},
			live: {
				files: {
					'css/style.min.css': ['css/style.css'],
					'css/ionic.min.css': ['lib/ionic/css/ionic.min.css']
				}
			}
		},
		copy: {
			base: {
				files: [{
					src: 'lib/ionic/js/ionic.bundle.min.js',
					dest: cordovaPath + 'www/js/ionic.bundle.min.js'
				}, {
					src: ['css/ionic.min.css'],
					dest: cordovaPath + 'www/css/ionic.min.css'
				}, {
					src: ['css/style.min.css'],
					dest: cordovaPath + 'www/css/style.min.css'
				}, {
					expand: true,
					cwd: 'lib/ionic/fonts',
					src: '**',
					dest: cordovaPath + 'www/fonts'
				}, {
					expand: true,
					cwd: 'img',
					src: '**',
					dest: cordovaPath + 'www/img'
				}, {
					expand: true,
					cwd: 'views',
					src: '**',
					dest: cordovaPath + 'www/views'
				},]
			},
			app: {
				files: [{
					src: 'build/app-babel.min.js',
					dest: cordovaPath + 'www/lib.js'
				}, {
					src: 'app.html',
					dest: cordovaPath + 'www/index.html'
				}]
			},
			debug: {
				files: [{
					src: 'build/app-debug.min.js',
					dest: cordovaPath + 'www/lib.js'
				},
					{
						src: 'build/app-debug-babel.min.js.map',
						dest: cordovaPath + 'www/app-debug-babel.min.js.map'
					}, {
						src: 'app-debug.html',
						dest: cordovaPath + 'www/index.html'
					}]
			}
		},
		watch: {
			sass: {
				options: {},
				files: ['lib/ionic/scss/*.scss'],
				tasks: ['sass']
			},
			less: {
				options: {},
				files: ['less/*.less'],
				tasks: ['less']
			},
			js: {
				options: {},
				files: ['src/*.js', 'src/*/*.js'],
				tasks: ['concat:dev', 'babel:dev']
			}
		},
		shell: {
			android: {
				command: [
					"cd " + cordovaPath,
					"cordova prepare android"
				].join('&&')
			},
			ios: {
				command: [
					"cd " + cordovaPath,
					"cordova prepare ios"
				].join('&&')
			}
		}
	});
	grunt.registerTask('weixin', ['less', 'cssmin', 'concat:weixin', 'babel:weixin', 'uglify:weixin']);
	grunt.registerTask('dev', ['less', 'cssmin', 'concat:dev', 'babel:dev']);
	grunt.registerTask('default', ['dev']);
	//Android环境打包
	grunt.registerTask('android', ['less', 'cssmin', 'concat:app', 'babel:app', 'uglify:app', 'copy:base', 'copy:app', 'shell:android']);
	//Android debug环境打包
	grunt.registerTask('android-debug', ['less', 'cssmin', 'concat:app', 'babel:debug', 'uglify:debug', 'copy:base', 'copy:debug', 'shell:android']);
	//iOS环境打包
	grunt.registerTask('ios', ['less', 'cssmin', 'concat:app', 'babel:app', 'uglify:app', 'copy:base', 'capy:app', 'shell:ios']);
};
