/**
 * @copyright Maichong Software Ltd. 2015 http://maichong.it
 */

'use strict';

var app = angular.module('app', ['ionic']);
var isDefined = angular.isDefined;
var isFunction = angular.isFunction;
var isString = angular.isString;
var isObject = angular.isObject;
var isArray = angular.isArray;
var isNumber = angular.isNumber;
var forEach = angular.forEach;
var extend = angular.extend;
var copy = angular.copy;
var $ = angular.element;
var noop = angular.noop;

//API根路径
var __ROOT = '/api/';
//__ROOT = 'http://192.168.31.175:3015/api/';
var __BUILD = '20151230';
var __V = '0.0.1';
var __P = window.__P;


app.s = app.service;
app.d = app.directive;
app.c = app.controller;

/**
 * 写入本地设置
 * @param key
 * @param value
 */
function setSettings(key, value) {
	window.localStorage && window.localStorage.setItem(key, JSON.stringify(value));
}

/**
 * 读取本地设置
 * @param key
 */
function getSettings(key) {
	if (window.localStorage) {
		try {
			return JSON.parse(window.localStorage.getItem(key));
		} catch (err) {
			return null;
		}
	}
}

//格式化数据
function round(val, precision) {
	val = parseFloat(val) || 0;
	return parseFloat(val.toFixed(precision));
}
//Angular 路由
app.config([
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	'$ionicConfigProvider',
	function ($stateProvider, $urlRouterProvider, $locationProvider, $ionicConfigProvider) {
		$stateProvider
			.state('index', {
				url: '/index',
				templateUrl: 'views/index.html'

			});
		$urlRouterProvider.otherwise('/index');

		//$locationProvider.html5Mode(true);
		//
		//平台默认配置
		$ionicConfigProvider.views.transition('ios');
		$ionicConfigProvider.templates.maxPrefetch(2);
		$ionicConfigProvider.backButton.text('返回');
		$ionicConfigProvider.backButton.previousTitleText(false);
		$ionicConfigProvider.tabs.position('bottom');
		$ionicConfigProvider.tabs.style('standard');
		$ionicConfigProvider.navBar.alignTitle('center');
	}
]);
