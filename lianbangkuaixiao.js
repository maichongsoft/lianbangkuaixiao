var dotenv = require('dotenv');
dotenv.load();

require('babel-register');

//配置
var config = require('./lib/config');
var keystone = require('keystone');

keystone.init(config.init);

keystone.import('models');


//自定义设置
var _ = require('underscore');
_.each(config.set, function (value, key) {
	keystone.set(key, value);
});

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	'用户': 'users'
});

keystone.start();
