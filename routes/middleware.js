/**
 * 脉冲软件
 * http://maichong.it
 * Created by Liang on 15/9/22.
 * liang@maichong.it
 */

'use strict';

const _ = require('underscore');
const keystone = require('keystone');
const User = keystone.list('User').model;
//const BaiduCache = keystone.list('BaiduCache').model;

//AJAX返回
function result(data, error, msg) {
	let result = {
		data: data || {},
		error: error || 0,
		msg: msg || ''
	};
	if (this.req.query.callback) {
		this.jsonp(result);
	} else {
		this.json(result);
	}
}

/**
 * 返回错误，自动判断是否是AJAX请求
 * @param msg  错误消息
 * @param code [optional] 错误码
 * @param isAjax [optional] 强制返回AJAX
 */
function error(msg, code, isAjax) {
	let headers = this.req.headers;
	if (isAjax === undefined) {
		if (this.req.query.ajax) {
			isAjax = true;
		} else if (headers.accept && headers.accept.indexOf('application/json') === 0) {
			isAjax = true;
		} else if (headers['x-requested-with'] === 'XMLHttpRequest') {
			isAjax = true;
		}
	}

	let message = typeof msg === 'string' ? msg : msg.message;
	if (isAjax) {
		//如果是AJAX请求，返回JSON
		code = code || 1;
		this.result({}, code, message);
	} else {
		//如果不是AJAX请求，返回HTML错误页面
		this.errorPage(message);
	}
}

//返回错误页面
function errorPage(msg, time, template) {
	let res = this;
	let req = this.req;
	let locals = res.locals;

	if (typeof time === 'string') {
		template = time;
		time = 10;
	}

	if (typeof time === 'undefined') {
		time = 10;
	}

	locals.time = time;

	msg = msg || '系统错误';

	locals.title = msg;

	locals.to = '/m/';

	locals.errorMsg = typeof msg === 'object' ? msg.message : msg;

	process.nextTick(function () {
		let view = new keystone.View(req, res);
		view.render(template || 'error');
	});

	let result = {
		template: function (template) {
			locals.template = template;
			return result;
		},
		to: function (url) {
			locals.to = url;
			return result;
		},
		time: function (time) {
			locals.time = time;
			return result;
		}
	};

	return result;
}

/**
 * 快速显示页面
 * @param template
 * @param locals [optional] 模板变量对象
 * @returns {*}
 */
function show(template, locals) {
	locals && _.extend(this.locals, locals);
	let view = new keystone.View(this.req, this);
	setImmediate(function () {
		view.render(template);
	});
	return view;
}

function getIP() {
	let ip = this.clientIP;

	if (!ip) {
		ip = this.headers['x-forwarded-for'] || this.ip;
		let matchs = ip.match(/(\d+)\.(\d+)\.(\d+)\.(\d+)/);
		if (!matchs) {
			return '';
		}
		ip = this.clientIP = matchs[0];
	}

	return ip;
}

function getUrl() {
	return 'http://' + this.headers.host + this.url;
}

exports.initLocals = function (req, res, next) {
	res._headers['x-powered-by'] = 'http://maichong.it';

	let locals = res.locals;

	locals.navLinks = [
		{label: 'Home', key: 'home', href: '/'}
	];

	locals.user = req.user;

	//通用返回JSON的方法
	res.result = result;
	//通用返回错误的方法
	res.error = error;
	//显示错误页面
	res.errorPage = errorPage;
	//快捷显示页面
	res.show = show;
	//获取IP
	req.getIP = getIP;
	//console.log(req);

	req.getUrl = getUrl;
	next();
};

/**
 Fetches and clears the flashMessages before a view is rendered
 */

exports.flashMessages = function (req, res, next) {
	let flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error')
	};

	res.locals.messages = _.any(flashMessages, function (msgs) {
		return msgs.length;
	}) ? flashMessages : false;

	next();
};

/**
 Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function (req, res, next) {
	if (!req.user) {
		res.redirect('/m/login');
	} else {
		if (req.user.deleted) {
			keystone.session.signout(req, res, function () {
				res.redirect('/m/login');
			});
		} else {
			next();
		}
	}
};

exports.api = async function (req, res, next) {
	if (req.query.callback) {
		_.extend(req.body, req.query);
	}
	let userId = req.body._u;
	let token = req.body._t;
	let _p = req.body._p;
	req.query.ajax = 1;
	if (_p !== 'wx') {
		req.user = null;
		if (userId && token) {
			let user = await User.findById(userId);
			if (user && (user.token + user.password.substring(30, 40)) == token) {
				req.user = user;
			}
		}
	}

	if (!req.body._lng) {
		//没有GPS信息
		let location = await BaiduCache.ipToLocation(req.getIP());
		if (location) {
			req.body._lng = location.lng;
			req.body._lat = location.lat;
		}
	}
	next();
};

exports.userApi = async function (req, res, next) {
	if (req.user) {
		return next();
	}
	console.log('#############', req.user);
	if (!req.user || req.user.blocked) {
		res.error('登录超时', 1000);
	} else {
		next();
	}
};

//企业权限
exports.enterpriseApi = async function (req, res, next) {
	if (req.user && req.user.enterpriseStatus === 2) {
		return next();
	}

	if (!req.user || req.user.blocked) {
		res.error('登录超时', 1000);
	}

	if (req.user.enterpriseStatus !== 2) {
		res.error('没有企业权限', 1001);
	}
	next();

};
