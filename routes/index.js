/**
 * 脉冲软件
 * http://maichong.it
 */

'use strict';

const keystone = require('keystone');
const middleware = require('./middleware');
const importRoutes = keystone.importer(__dirname);

keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

const views = importRoutes('./views');

module.exports = function (app) {
	//模版路由
	app.get('/', views.index);

	//API
	app.all('/api/*', middleware.api);
	app.all(/\/api\/(user)/, middleware.userApi);
	app.all('/api/:controller/:action', dispatcher('api'));

	//计划任务
	//app.get('/cron/:controller/:action', dispatcher('cron'));
	app.all('*', function (req, res) {
		res.status(404);
		res.errorPage('没有找到指定页面', 5);
	});
};

/**
 * 调度器
 * @param group 控制器组
 * @returns {Function} 返回路由监听器
 */
function dispatcher(group) {
	//该控制器组下所有的操作
	let actions = importRoutes('./' + group);
	return function (req, res, next) {
		let controller = req.params.controller;
		let action = req.params.action;
		if (!actions[controller] || !actions[controller][action]) {
			return res.error('ERROR METHOD');
		}
		actions[controller][action](req, res, next);
	};
}
